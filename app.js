//Выводим текст Canvas

let canvas = document.getElementById('canvas');
let ctx = canvas.getContext('2d');
let width = 1500;
let height = 800;

let mouse = {
    x: 0,
    y: 0
}
let selected = false;

canvas.width = width;
canvas.height = height;
canvas.style.background = 'seagreen';
ctx.fillStyle = 'blue';
ctx.strokeStyle = 'yellow';
ctx.lineWidth = 5;


let Rect = function (x, y, w, h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
};

Rect.prototype = {
    draw: function () {
        ctx.fillRect(this.x, this.y, this.w, this.h)
    },
    stroke: function () {
        ctx.strokeRect(this.x, this.y, this.w, this.h)
    }
};

let isCursorInRect = function (rect) {
    return mouse.x > rect.x && mouse.x < rect.x + rect.w && mouse.y > rect.y && mouse.y < rect.y + rect.h;
}

let i;
let rect = [];
let rectValue = 10;

for (i = 0; i < rectValue; i++) {
    rect.push(
        new Rect(25 + i * (50 + 60), 25, 100, 100)
    );
}

setInterval(function () {
    ctx.clearRect(0, 0, width, height)
    for (i in rect) {
        rect[i].draw();

        if (isCursorInRect(rect[i])) {
            rect[i].stroke();
        }
    }

    if (selected) {
        selected.x = mouse.x - selected.w / 2;
        selected.y = mouse.y - selected.h / 2;
    }
}, 30);

window.onmousemove = function (e) {
    mouse.x = e.pageX;
    mouse.y = e.pageY;
};

window.onmousedown = function () {
    if (!selected) {
        let i;
        for (i in rect) {
            rect[i].draw();
            if (isCursorInRect(rect[i])) {
               selected = rect[i];
            }
        }
    }
};

window.onmouseup = function () {
    selected = false;
};

//
// let canvas = document.getElementById('btn-line');
// let ctx = canvas.getContext('2d');
// let width = 1500;
// let height = 800;
//
// let btnRetry = {
//     x:0,
//     y:0,
//     w:120,
//     h:40,
//     text:"Retry",
//     state:"default",
//     draw: function() {
//         ctx.font = "20px Arial ";
//         switch(this.state){
//             case "over":
//                 ctx.fillStyle = "darkred";
//                 ctx.fillRect(this.x,this.y,this.w,this.h);
//                 ctx.fillStyle = "black";
//                 ctx.fillText("Retry?",this.x+this.w/2 - ctx.measureText("Retry").width/2,this.y+this.h/2+10 );
//                 break;
//             default:
//                 ctx.fillStyle = "red";
//                 ctx.fillRect(this.x,this.y,this.w,this.h);
//                 ctx.fillStyle = "black";
//                 ctx.fillText("Retry",this.x+this.w/2 - ctx.measureText("Retry").width/2,this.y+this.h/2+10 );
//         }
//     }
//
//
// };
// btnRetry.draw();
// canvas.addEventListener("mousedown",function(e){
//     if(checkCollision(e.offsetX,e.offsetY,btnRetry ))
//         alert("Retrying!")
// },false);
//
//
// canvas.addEventListener("mousemove",function(e){
//     btnRetry.state = checkCollision(e.offsetX,e.offsetY,btnRetry )?"over":"def";
//     btnRetry.draw();
// },false);
//
// function checkCollision(x,y,obj){//Проверяет входит ли точка в  прямоугольник
//     return x >= obj.x && x <= obj.x + obj.w &&
//         y >= obj.y && y <= obj.y + obj.h ;
//
// }





























// let btn = document.getElementById('butt');
// btn.onclick = function () {
//     ctx.clearRect(0, 0, canvas.width, canvas.height)
//     let inputText = document.getElementById('input').value;
//     ctx.fillText(inputText, canvas.width / 2, canvas.height / 2);
//     console.log('Введено:', inputText);
//
// }












